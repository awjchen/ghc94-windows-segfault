module Main (main) where

import Data.Foldable (for_)
import Foreign.C.String (withCAStringLen)
import System.IO

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  for_ [(1 :: Int)..20] $ \i -> do
    let len = 2^i
        str = replicate len 'a'
    print i
    withCAStringLen str $ \_ -> pure ()
